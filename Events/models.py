
from django.db import models

# Create your models here.

class FormMember(models.Model):
    FirstName = models.CharField(max_length=20)
    LastName = models.CharField(max_length=20)

    def __str__(self):
        return self.FirstName

class FormEvent(models.Model):
    EventName = models.CharField(max_length=20)

    def __str__(self):
        return self.EventName