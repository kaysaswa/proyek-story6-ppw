from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import EventForm
from .models import FormMember,FormEvent

# Create your views here.
response={}
def Events(request):
    return render(request,'Events/AddEvent.html',response)

def AddEvent(request):
    model = FormEvent
    form = EventForm(request.POST or None)

    if(request.method=='POST' and form.is_valid()):
        response['EventName']=request.POST['Event']
        Event = EventForm(EventName=response['EventName'])
        Event.save()
        return HttpResponseRedirect('')
    else:
        return HttpResponseRedirect('')
    
    return render(request,'Events/AddEvent.html',response)  