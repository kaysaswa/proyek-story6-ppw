from django.urls import path
from . import views
from Events.views import Events,AddEvent

app_name = 'story6'

urlpatterns=[
    path('',Events,name="Events"),
    path('AddEvent',AddEvent,name='AddEvent')
]