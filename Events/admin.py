from django.contrib import admin

# Register your models here.

from .models import FormMember,FormEvent
admin.site.register(FormMember)
admin.site.register(FormEvent)